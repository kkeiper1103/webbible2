<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BooksTest extends TestCase
{
    /**
     *
     */
    public function testIndexMethod() {
        $this->json('GET', route('json.books.index'))
            ->seeJsonStructure([
                '*' => ['id', 'name']
            ]);
    }

    /**
     * Ensure that the show method returns a 301 redirect to the chapters index method
     */
    public function testShowMethod() {
        $expected = 301;
        $actual = $this->call('GET', route('json.books.show', 1))->status();

        $this->assertEquals($expected, $actual, 'BooksController@show should return a 301 Redirect.');
    }
}
