<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChaptersTest extends TestCase
{
    /**
     *
     */
    public function testIndexMethod() {
        $this->json('GET', route('json.books.chapters.index', 1))
            ->seeJsonStructure([
                'id',
                'name',
                'chapters' => [
                    '*' => [
                        'id', 'number'
                    ]
                ]
            ]);
    }

    /**
     * Ensure that the show method returns a 301 redirect to the verses index method
     */
    public function testShowMethod() {
        $expected = 301;
        $actual = $this->call('GET', route('json.books.chapters.show', [1, 1]))->status();

        $this->assertEquals($expected, $actual, 'ChaptersController@show should return a 301 Redirect.');
    }
}
