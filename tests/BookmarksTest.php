<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookmarksTest extends TestCase
{
    use DatabaseTransactions;

    /**
     *
     */
    public function testIndexMethod() {
        $user = $this->getTestingUser();

        $user->bookmarks()->create([
            'verse_id' => 15555
        ]);

        $this->actingAs($user)
            ->json('GET', route('json.bookmarks.index'))
            ->seeJsonStructure([
                '*' => [
                    'id', 'user_id', 'verse_id', 'verse'
                ]
            ]);
    }

    /**
     *
     */
    public function testStoreBookmarkMethod() {
        $user = $this->getTestingUser();

        $this->actingAs($user)
            ->post(route('json.bookmarks.store'), [
                'verse_id' => 1853
            ]);

        $this->seeInDatabase('bookmarks', [
            'verse_id' => 1853,
            'user_id' => $user->id
        ]);
    }

    /**
     *
     */
    public function testShowBookmarkMethod() {
        $user = $this->getTestingUser();

        $bm = $user->bookmarks()->firstOrCreate([
            'verse_id' => 15555
        ]);

        $this->actingAs($user)
            ->json('GET', route('json.bookmarks.show', $bm->id))
            ->seeJsonStructure([
                'id', 'user_id', 'verse_id', 'verse'
            ]);
    }

    /**
     *
     */
    public function testDestroyBookmarkMethod() {
        $user = $this->getTestingUser();

        $bookmark = $user->bookmarks()->create([
            'verse_id' => 1555
        ]);

        //
        $this->actingAs($user)
            ->delete(route('json.bookmarks.destroy', $bookmark->id));

        $this->dontSeeInDatabase('bookmarks', [
            'verse_id' => 1555,
            'user_id' => $user->id
        ]);
    }
}
