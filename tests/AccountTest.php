<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccountTest extends TestCase
{
    use DatabaseTransactions;

    /**
     *
     */
    public function testSaveUserAccountMethod() {
        $user = $this->getTestingUser();

        $params = [
            'name' => 'kyle.keiper1992',
            'email' => 'kkeiper@starkstate.edu',

            'current_password' => $this->authorizedPassword(),
            'password' => 'P@ssw0rd',
            'password_confirmation' => 'P@ssw0rd',

            'stylesheet' => 'https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/journal/bootstrap.min.css',
            'connection' => 'web',
            'timezone' => 'America/Dawson',
            'allow_gestures' => true
        ];

        $this->actingAs($user)
            ->post('account', $params);


        // now, assert the params have been applied
        $u = App\User::find($user->id);


        $this->assertEquals($params['name'], $u->name, "User's Name Should be Updated.");
        $this->assertEquals($params['email'], $u->email, "User's Email Should be Updated.");

        $this->assertTrue( Auth::validate(['email' => $u->email, 'password' => $params['password']]), "New Password Should Be Applied After POST to /account." );


        $this->assertEquals($params['stylesheet'], $u->preferences->stylesheet, "User's Preferred Stylesheet Should be Updated.");
        $this->assertEquals($params['connection'], $u->preferences->connection, "User's Preferred Connection Should be Updated.");
        $this->assertEquals($params['timezone'], $u->preferences->timezone, "User's Preferred Timezone Should be Updated.");

        $this->assertEquals($params['allow_gestures'], $u->preferences->allow_gestures, "\"Allow Gestures\" Should be True.");
    }

    /**
     *
     */
    public function testRegistrationRoute() {
        $params = [
            'name' => 'new.registration',
            'email' => 'new-reg@example.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $this->post('register', $params);


        $this->seeInDatabase('users', array_except($params, ['password', 'password_confirmation']));
    }
}
