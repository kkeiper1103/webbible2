<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class JsonTest extends TestCase
{

    /**
     *
     */
    public function testBookListingJson() {

        /**
         *
         */
        $this->json('GET', '/json/books')
            ->seeJsonStructure([
                '*' => [
                    'id', 'name', 'created_at', 'updated_at'
                ]
            ]);
    }

    /**
     *
     */
    public function testBookChaptersJson() {

        $this->json('GET', '/json/books/1/chapters')
            ->seeJsonStructure([
                'name',
                'chapters' => [
                    '*' => [
                        'book_id', 'number'
                    ]
                ]
            ]);
    }

    /**
     *
     */
    public function testBookChapterVersesJson() {

        $this->json('GET', '/json/books/1/chapters/1/verses')
            ->seeJsonStructure([
                'number',
                'book' => [
                    'name', 'id'
                ],
                'verses' => [
                    '*' => [
                        'number', 'value'
                    ]
                ]
            ]);
    }

    /**
     *
     */
    public function testGuestUserJson() {

        $this->json('GET', '/json/user')
            ->seeJsonEquals([
                'recent_chapters' => []
            ]);
    }

    /**
     *
     */
    public function testAuthenticatedUserJson() {
        // dunno about this
        $user = $this->getTestingUser();


        $this->actingAs($user)
            ->json('GET', '/json/user')
            ->seeJsonStructure([
                'id', 'name', 'email',
                'recent_chapters' => []
            ]);
    }
}
