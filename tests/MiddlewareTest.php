<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MiddlewareTest extends TestCase
{
    /**
     *
     */
    public function testSetBibleVersionDefault() {
        $this->call('GET', '/');

        $expected = 'kjv';
        $actual = $this->app->make('bible.version');

        $this->assertEquals($expected, $actual, "The Bible Version Should be KJV By Default.");
    }

    /**
     *
     */
    public function testSetBibleVersionToAsv() {
        $this->call('GET', '/', ['version' => 'asv']);

        $expected = 'asv';
        $actual = $this->app->make('bible.version');

        $this->assertEquals($expected, $actual, "The Bible Version Should be ASV On This Request.");
    }

    /**
     *
     */
    public function testSetBibleVersionToWeb() {
        $this->call('GET', '/', ['version' => 'web']);

        $expected = 'web';
        $actual = $this->app->make('bible.version');

        $this->assertEquals($expected, $actual, "The Bible Version Should be WEB On This Request.");
    }

    /**
     *
     */
    public function testSetBibleVersionToRsv() {
        $this->call('GET', '/', ['version' => 'rsv']);

        $expected = 'rsv';
        $actual = $this->app->make('bible.version');

        $this->assertEquals($expected, $actual, "The Bible Version Should be RSV On This Request.");
    }

    /**
     *
     */
    public function testSetBibleVersionToTyndale() {
        $this->call('GET', '/', ['version' => 'tyndale']);

        $expected = 'tyndale';
        $actual = $this->app->make('bible.version');

        $this->assertEquals($expected, $actual, "The Bible Version Should be Tyndale On This Request.");
    }

    /**
     *
     */
    public function testPreferredVersionDoesNotOverrideRequestVersion() {
        $user = $this->getTestingUser();

        if( $user->preferences == NULL )
            $user->preferences = new \App\Preferences();

        $user->preferences->connection = 'asv';

        // make sure that preferred version does not override $_REQUEST version
        $this->actingAs($user)
            ->call('GET', '/', ['version' => 'tyndale']);

        //
        $expected = 'tyndale';
        $actual = $this->app->make('bible.version');

        $this->assertEquals($expected, $actual, "The Bible Version Should be Tyndale Because Request Overrides Preferences.");
    }


    /**
     *
     */
    public function testUsesPreferredVersion() {
        $user = $this->getTestingUser();

        if( $user->preferences == NULL )
            $user->preferences = new \App\Preferences();

        $user->preferences->connection = 'asv';

        $this->actingAs($user)
            ->call('GET', '/');

        //
        $expected = 'asv';
        $actual = $this->app->make('bible.version');

        $this->assertEquals($expected, $actual, "The Bible Version Should be ASV Due to Authenticated User's Preferences.");
    }



    /**
     *
     */
    public function testLogRecentChapterOnNewChapter() {
        $user = $this->getTestingUser();
        \DB::table('recent_chapters')->where('user_id', '=', $user->id)->delete();

        $chapter = json_decode('{
            "book_id": "61",
            "chapter_number": "1",
            "reference_name": "2 Peter 1",
            "created_at": "2016-04-19 11:06:52",
            "updated_at": "2016-04-19 11:06:52"
        }');

        //
        $this->actingAs($user)
            ->get("/json/books/{$chapter->book_id}/chapters/{$chapter->chapter_number}/verses");

        // make sure the middleware logged the new chapter
        $this->seeInDatabase('recent_chapters', [
            'user_id' => $user->id,
            'reference_name' => "2 Peter 1"
        ]);

        \DB::table('recent_chapters')->where('user_id', '=', $user->id)->delete();
    }

    /**
     *
     */
    public function testLogRecentChapterOnRevisitedChapter() {
        $user = $this->getTestingUser();
        \DB::table('recent_chapters')->where('user_id', $user->id)->delete();

        $chapter = json_decode('{
            "book_id": "61",
            "chapter_number": "1",
            "reference_name": "2 Peter 1",
            "created_at": "2016-04-19 11:06:52",
            "updated_at": "2016-04-19 11:06:52"
        }');

        //
        $this->actingAs($user)
            ->get("/json/books/{$chapter->book_id}/chapters/{$chapter->chapter_number}/verses");

        $expected = 1;

        //
        $this->actingAs($user)
            ->get("/json/books/{$chapter->book_id}/chapters/{$chapter->chapter_number}/verses");

        $actual = count( $this->getTestingUser()->recent_chapters() );

        $this->assertEquals($expected, $actual,
            "Revisiting a Chapter Should Not Log the Chapter in Recent Chapters Twice.");

        \DB::table('recent_chapters')->where('user_id', $user->id)->delete();
    }
}
