<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotesTest extends TestCase
{
    use DatabaseTransactions;

    /**
     *
     */
    public function testIndexMethod() {
        $this->app->singleton('bible.version', function($app) { return 'kjv'; });
        $this->getTestingUser()->notes()->save( factory(App\Note::class)->make() );

        $this->actingAs($this->getTestingUser())
            ->json('GET', route('json.notes.index'))
            ->seeJsonStructure([
                '*' => [
                    'id', 'body', 'verse_id'
                ]
            ]);
    }

    /**
     *
     */
    public function testStoreMethod() {
        $this->app->singleton('bible.version', function($app) { return 'kjv'; });
        $note = factory(App\Note::class)->make();

        $this->actingAs($this->getTestingUser())
            ->post(route('json.notes.store'), $note->getAttributes());

        $this->seeInDatabase('notes', $note->getAttributes());
    }

    /**
     *
     */
    public function testShowMethod() {
        $this->app->singleton('bible.version', function($app) { return 'kjv'; });
        $note = $this->getTestingUser()->notes()->save( factory(App\Note::class)->make() );

        $this->actingAs($this->getTestingUser())
            ->json('GET', route('json.notes.show', $note->id))
            ->seeJsonStructure([
                'id', 'body', 'verse_id'
            ]);
    }

    /**
     *
     */
    public function testUpdateMethod() {
        $this->app->singleton('bible.version', function($app) { return 'kjv'; });
        $note = $this->getTestingUser()->notes()->save( factory(App\Note::class)->make() );

        $body = "Lorem Ipsum Dolor Set Amit.";

        $this->actingAs($this->getTestingUser())
            ->patch(route('json.notes.update', $note->id), [
                'body' => $body
            ]);

        $this->seeInDatabase('notes', [
            'id' => $note->id,
            'body' => $body
        ]);
    }

    /**
     *
     */
    public function testDestroyMethod() {
        $this->app->singleton('bible.version', function($app) { return 'kjv'; });
        $note = $this->getTestingUser()->notes()->save( factory(App\Note::class)->make() );

        $this->actingAs($this->getTestingUser())
            ->delete(route('json.notes.destroy', $note->id));

        $this->dontSeeInDatabase('notes', [
            'id' => $note->id
        ]);
    }
}
