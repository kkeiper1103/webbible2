<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VersesTest extends TestCase
{
    /**
     *
     */
    public function testIndexMethod() {
        $this->json('GET', route('json.books.chapters.verses.index', [1, 1]))
            ->seeJsonStructure([
                'id', 'number', 'book_id',
                'book' => [
                    'id', 'name'
                ],
                'verses' => [
                    '*' => [
                        'id', 'number', 'value'
                    ]
                ]
            ]);
    }

    /**
     *
     */
    public function testShowMethod() {
        $this->json('GET', route('json.books.chapters.verses.show', [1, 1, 1]))
            ->seeJsonStructure([
                'id', 'number', 'value'
            ]);
    }
}
