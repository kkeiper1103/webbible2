<?php

use Illuminate\Database\Seeder;

class BookmarksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function() {
            DB::table('bookmarks')->truncate();

            $bookmarks = array(
                array('id' => '1','user_id' => '3','verse_id' => '29903','created_at' => '2014-06-15 03:08:28','updated_at' => '2014-06-15 03:08:28'),
                array('id' => '2','user_id' => '1','verse_id' => '29714','created_at' => '2014-07-03 09:32:16','updated_at' => '2014-07-03 09:32:16'),
                array('id' => '3','user_id' => '3','verse_id' => '28484','created_at' => '2014-07-10 02:53:39','updated_at' => '2014-07-10 02:53:39'),
                array('id' => '4','user_id' => '3','verse_id' => '28485','created_at' => '2014-07-10 02:53:44','updated_at' => '2014-07-10 02:53:44'),
                array('id' => '5','user_id' => '3','verse_id' => '17511','created_at' => '2014-07-10 02:59:07','updated_at' => '2014-07-10 02:59:07'),
                array('id' => '6','user_id' => '3','verse_id' => '29446','created_at' => '2014-08-02 09:44:15','updated_at' => '2014-08-02 09:44:15'),
                array('id' => '8','user_id' => '3','verse_id' => '29449','created_at' => '2014-08-02 09:45:58','updated_at' => '2014-08-02 09:45:58'),
                array('id' => '9','user_id' => '3','verse_id' => '30449','created_at' => '2014-10-08 02:39:15','updated_at' => '2014-10-08 02:39:15'),
                array('id' => '10','user_id' => '3','verse_id' => '18746','created_at' => '2014-11-07 03:39:27','updated_at' => '2014-11-07 03:39:27'),
                array('id' => '11','user_id' => '3','verse_id' => '18130','created_at' => '2014-11-07 11:16:31','updated_at' => '2014-11-07 11:16:31'),
                array('id' => '14','user_id' => '3','verse_id' => '30460','created_at' => '2014-12-27 03:10:53','updated_at' => '2014-12-27 03:10:53'),
                array('id' => '15','user_id' => '1','verse_id' => '20311','created_at' => '2015-06-18 17:45:27','updated_at' => '2015-06-18 17:45:27'),
                array('id' => '16','user_id' => '1','verse_id' => '20310','created_at' => '2015-06-18 17:45:30','updated_at' => '2015-06-18 17:45:30'),
                array('id' => '17','user_id' => '3','verse_id' => '17756','created_at' => '2015-10-04 13:08:40','updated_at' => '2015-10-04 13:08:40'),
                array('id' => '21','user_id' => '3','verse_id' => '23726','created_at' => '2016-02-17 02:23:44','updated_at' => '2016-02-17 02:23:44')
            );

            foreach($bookmarks as $b) {
                $bm = new \App\Bookmark([
                    'verse_id' => $b['verse_id']
                ]);

                $bm->id = $b['id'];
                $bm->user_id = $b['user_id'];
                $bm->created_at = $b['created_at'];
                $bm->updated_at = $b['updated_at'];

                $bm->save();
            }
        });
    }
}
