<?php

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::transaction(function() {
            DB::table('preferences')->truncate();
            DB::table('users')->truncate();

            $users = array(
                array('id' => '1','username' => 'kkeiper1103','hashed_password' => '$2y$08$Yz25A2KaDc5Zc6UvWETRcuTraxCVQc35Eb0LCcEaZnFQTP1vNdOSe','email' => 'goldentoa11@gmail.com','perishable_token' => 'svLNIhfU81V9MABJk6SEupdzJ1RH0IFq','api_token' => 'KvQrcIU5nxdTe71gERmoc5InfIFeQrV8','active' => '1','created_at' => '2014-05-10 13:28:01','updated_at' => '2015-06-18 17:45:41','remember_token' => 'k5jfFlv8xGvo55BkpF6THLonrpji3ELAgxa0OZGCacwLVmPJCCDlNOmsfDwE','theme_id' => 'darkly.min.css','profile_settings' => 'O:15:"ProfileSettings":1:{s:12:"last_chapter";s:4:"1121";}'),
                array('id' => '2','username' => 'mellamokb','hashed_password' => '$2y$08$uCkimZD7bJaX9EmcxJ.mtueSjciwTf8OptnC94PiTlT4FS89Fo3ea','email' => 'kbachtold@gmail.com','perishable_token' => 'GP8N6Zk4aGvl2GT9tdlZUG0PvdXLa2hS','api_token' => '9ZUbbIULu6wrYdi2qEy6gngPuSegxCqj','active' => '0','created_at' => '2014-05-15 22:05:24','updated_at' => '2014-05-15 22:05:24','remember_token' => '','theme_id' => 'default.min.css','profile_settings' => ''),
                array('id' => '3','username' => 'bud1960','hashed_password' => '$2y$08$S.lZnnO6zdVDtSkPVU4qMeztjsne9p2InPWEUxAPeEZIulx9G9Jv6','email' => 'manz.bud@gmail.com','perishable_token' => 'RBumKo1NnJiASrEIBztsy3WftjkqV3XC','api_token' => 'l0AeCM0E6x28Az8GtBXDJbQfHqlR5CWv','active' => '0','created_at' => '2014-06-15 03:06:48','updated_at' => '2016-02-18 11:51:46','remember_token' => '','theme_id' => 'cerulean.min.css','profile_settings' => 'O:15:"ProfileSettings":1:{s:12:"last_chapter";s:3:"531";}'),
                array('id' => '6','username' => 'kkeiper','hashed_password' => '$2y$10$JHRN3R/5Egz20FoSAdjymupIzVlNIZsTaaxBG66IR5By/P85mIVpm','email' => 'kkeiper@wrladv.com','perishable_token' => 'gOBN7MjI1BuL38EpVMvqEWSB5OfN3imw','api_token' => '0p8jjHdai8rQ0RPLRBp9kaL5UXzXfZoW','active' => '1','created_at' => '2014-07-19 01:40:04','updated_at' => '2014-07-19 01:53:13','remember_token' => 'Ej49IT4BncnvRzlgV43KBoasfJGs8bojI717YLJbrVuU4X45HjdupHOxtMjN','theme_id' => 'default.min.css','profile_settings' => 'O:15:"ProfileSettings":1:{s:12:"last_chapter";s:1:"1";}'),
                array('id' => '7','username' => 'gblunier','hashed_password' => '$2y$10$pG8CN0DqGRxjOtfZ0fUUV.BCOACgnTMSWvFsF0hLTBmWMenYHfwVO','email' => 'gblunier@gmail.com','perishable_token' => 'LNkTQiKWOlItztvaoZUPQG0NGPCbTYYW','api_token' => '8Ve5gXsBawSZs8b2UEtc84Gd3VYGD1Sr','active' => '1','created_at' => '2016-01-27 14:05:23','updated_at' => '2016-01-27 14:07:35','remember_token' => 'lCEiPkWgLg8gIrIWnyy2QOcNiSpii1U4KpUqXRkmCmGqB87T1puMRdZSG80t','theme_id' => 'default.min.css','profile_settings' => 'O:15:"ProfileSettings":1:{s:12:"last_chapter";s:4:"1020";}')
            );

            //
            foreach( $users as $u ) {
                $user = new App\User([
                    'name' => $u['username'],
                    'email' => $u['email']
                ]);

                $user->id = $u['id'];
                $user->password = $u['hashed_password'];
                $user->created_at = $u['created_at'];
                $user->updated_at = $u['updated_at'];

                $user->save();

                $user->preferences()->create([]);
            }
        });
    }
}
