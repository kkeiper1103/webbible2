<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();
        DB::getPdo()->exec("SET FOREIGN_KEY_CHECKS = 0;");

        $this->call(UsersTableSeeder::class);
        $this->call(BookmarksTableSeeder::class);

        DB::getPdo()->exec('SET FOREIGN_KEY_CHECKS = 1;');
        \Illuminate\Database\Eloquent\Model::reguard();
    }
}
