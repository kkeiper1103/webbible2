<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveConnectionFromBookmarksAndNotesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function() {
            Schema::table('bookmarks', function($t) {
                $t->dropColumn('connection');
            });

            Schema::table('notes', function($t) {
                $t->dropColumn('connection');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function() {
            Schema::table('bookmarks', function($t) {
                $t->string('connection', 30)->after('verse_id')->nullable();
            });

            Schema::table('notes', function($t) {
                $t->string('connection', 30)->after('verse_id')->nullable();
            });
        });
    }
}
