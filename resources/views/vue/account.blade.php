<div class="account-form">
    <form action="{{ url('account') }}" class="form" method="post">
        {!! csrf_field() !!}

        <fieldset>
            <legend>General Settings</legend>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Name</label>

                <input type="text" class="form-control" name="name" id="name" value="@{{ user.name }}" />

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">Email Address</label>

                <input type="email" class="form-control" name="email" id="email" :value="user.email" />

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                <label for="current_password" class="control-label">Current Password</label>

                <input type="password" class="form-control" name="current_password" id="current_password" />

                @if ($errors->has('current_password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('current_password') }}</strong>
                    </span>
                @endif
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">Password</label>

                        <input type="password" class="form-control" name="password" id="password" />

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation" class="control-label">Password Confirmation</label>

                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" />

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

        </fieldset>

        <fieldset>
            <legend>Preferences</legend>

            <div class="form-group{{ $errors->has('stylesheet') ? ' has-error' : '' }}">
                <label for="stylesheet" class="control-label">Stylesheet</label>

                <select name="stylesheet" id="stylesheet" class="form-control">
                    <option value="">No Theme</option>
                @foreach( config('bootswatch.stylesheets') as $name => $stylesheet )
                    <option value="{{ $stylesheet }}"{{ Auth::check() && Auth::user()->preferences && Auth::user()->preferences->stylesheet == $stylesheet ? ' selected' : '' }}>{{ $name }}</option>
                @endforeach
                </select>

                @if ($errors->has('stylesheet'))
                    <span class="help-block">
                        <strong>{{ $errors->first('stylesheet') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('connection') ? ' has-error' : '' }}">
                <label for="connection" class="control-label">Bible Version</label>

                <select name="connection" id="connection" class="form-control">
                @foreach( collect(config('database.connections'))->slice(1) as $conn => $settings )
                    <option value="{{ $conn }}"{{ Auth::check() && Auth::user()->preferences && Auth::user()->preferences->connection == $conn ? ' selected' : '' }}>{{ $settings['title'] }}</option>
                @endforeach
                </select>

                @if ($errors->has('connection'))
                    <span class="help-block">
                        <strong>{{ $errors->first('connection') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group{{ $errors->has('timezone') ? ' has-error' : '' }}">
                <label for="timezone" class="control-label">Preferred Timezone</label>

                <select name="timezone" id="timezone" class="form-control">
                @foreach( timezone_identifiers_list() as $timezone )
                    <option{{ (Auth::check() && Auth::user()->preferences && Auth::user()->preferences->timezone == $timezone) ? ' selected' : '' }}>{{ $timezone }}</option>
                @endforeach
                </select>

                @if ($errors->has('timezone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('timezone') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('allow_gestures') ? ' has-error' : '' }}">
                <label for="allow_gestures" class="control-label">
                    <input type="checkbox" name="allow_gestures" id="allow_gestures" value="1" v-model="gestures"
                        {{ Auth::check() && Auth::user()->preferences && Auth::user()->preferences->allow_gestures == 1 ? " checked": '' }}> Allow Gestures?
                </label>

                <div class="help-block" v-if="gestures">
                    <p>Gestures is a feature that requires the use of a webcam. Similar to the swipe mechanic, gestures will track movement via your webcam
                    in order to move between books and chapters. Swipe your hand in front of the webcam in order to use this feature.</p>

                    <ul class="list-unstyled">
                        <li>Swipe Left: Move Forward One Chapter</li>
                        <li>Swipe Right: Move Backward One Chapter</li>
                        <li>Swipe Up: Move Forward One Book</li>
                        <li>Swipe Down: Move Backward One Book</li>
                    </ul>
                </div>

                @if($errors->has('allow_gestures'))
                    <span class="help-block">
                        <strong>{{ $errors->first('allow_gestures') }}</strong>
                    </span>
                @endif
            </div>

        </fieldset>

        <button class="btn btn-primary"><i class="fa fa-save"></i> Save Settings</button>
    </form>
</div>
