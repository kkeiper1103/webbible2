<div class="bookmarks" v-if="user.name != null">
    <h3 class="title"><i class="fa fa-bookmark"></i> Bookmarks</h3>

    <ul class="list-unstyled">
        <li v-for="b in bookmarks">
            <span data-toggle="tooltip" title="Remove Bookmark" v-on:click="deleteBookmark(b)"
                  class="label label-default remove-bookmark"><i class="fa fa-trash-o"></i></span>

            <a v-link="{ path: '/books/'+ b.verse.chapter.book_id +'/chapters/'+ b.verse.chapter.number +'/verses/'+ b.verse.number }">@{{ b }}</a>
        </li>
    </ul>
</div>
