
<ul class="list-inline" id="chapters-list" v-show="shown">
    <li v-for="c in book.chapters">
        <a class="label label-default"
           v-link="{ name: 'verses', params: { book_id: book.id, chapter_id: c.number }, query: $route.query, activeClass: 'label-success' }">@{{ c.number }}</a>
    </li>
</ul>

<div v-else>
    <a @click='toggle' class='label label-primary'>Show Chapters</a>
</div>

<router-view></router-view>