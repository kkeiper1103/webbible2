<div id="active-chapter" transition="bounce" transition-mode="in-out" class="bounce">
    <div class="wrapper">
        <h3 class="title">@{{ chapter.book.name }} @{{ chapter.number }}</h3>
        <div class="verses">
            <ol>
                <li v-for="v in chapter.verses">

                    <span v-bind:class="{ bold: (v.number == current_verse), italic: (v.number == current_verse) }">@{{ v.value }}</span>

                    <span v-if="logged_in" v-on:click="createBookmark(v)"
                          title="Bookmark This Verse" data-toggle="tooltip" class="label label-default bookmark"><i class="fa fa-bookmark-o"></i> </span>

                </li>
            </ol>
        </div>


        <span data-toggle="fullscreen">
            <i class="fa fa-expand"></i> Expand
        </span>




        <div class="back-to-top bg-success">
            Back to Top
        </div>
    </div>
</div>
