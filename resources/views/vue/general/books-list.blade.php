<ul class="list-inline" id="books-list" v-show="shown">
    <li v-for="b in books">
        <a v-link="{ name: 'chapters', params: { book_id: b.id }, activeClass: 'label-success', query: $route.query }"
           class="label label-default">@{{ b.name }}</a>
    </li>
</ul>
<div v-else>
    <p><a @click='toggle' class='label label-primary'>Show Books</a></p>
</div>