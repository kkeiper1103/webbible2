<search-form></search-form>

<div id="search-results" v-if="results.query">

    <div id="exact-matches" v-show="results.exact.length">
        <h2 class="title">Exact Matches <small>@{{ results.exact.length }} Results</small></h2>
        <span class="help-block" v-if="results.exact.length == 50">More Results Available. Please Narrow Search Terms.</span>

        <div class="media" v-for="r in results.exact">
            <a class="media-body" v-link="{ path: '/books/' + r.chapter.book_id + '/chapters/' + r.chapter.number }">
                <h4 class="media-heading">@{{ r.chapter.book.name }} @{{ r.chapter.number }}:@{{ r.number }}</h4>

                @{{{ r.value | embolden results.query.terms }}}
            </a>
        </div>
    </div>

    <div id="matches" v-show="results.partial.length">
        <h2 class="title">Partial Matches <small>@{{ results.partial.length }} Results</small></h2>
        <span class="help-block" v-if="results.partial.length == 50">More Results Available. Please Narrow Search Terms.</span>

        <div class="media" v-for="r in results.partial">
            <a class="media-body" v-link="{ path: '/books/' + r.chapter.book_id + '/chapters/' + r.chapter.number }">
                <h4 class="media-heading">@{{ r.chapter.book.name }} @{{ r.chapter.number }}:@{{ r.number }}</h4>

                @{{{ r.value | embolden results.query.terms }}}
            </a>
        </div>
    </div>

    <div id="no-matches" v-show="results.exact.length == 0 && results.partial.length == 0">
        <h2 class="title">No Matching Results Were Found.</h2>
    </div>
</div>

