<div class="recent-chapters" v-if="chapters.length">
    <h3 class="title"><i class="fa fa-bars"></i> Recent Readings</h3>

    <ul class="list-unstyled">
        <li v-for="c in chapters" data-toggle="tooltip" title="@{{ c.created_at | date 'MMM D @ h:mm A' }}" data-placement="left">
            <a v-link="{ path: '/books/'+ c.book_id +'/chapters/'+ c.chapter_number }">@{{ c.reference_name }}</a>
        </li>
    </ul>
</div>