<books-list></books-list>

<h1 class="title">Welcome to the Web Bible!</h1>
<div class="content">
    <p>My hope and prayer is that you find this tool useful and encouraging. If you have any concerns, questions or suggestions,
    please feel free to <a href="mailto:&#107;&#107;&#101;&#105;&#112;&#101;&#114;&#049;&#049;&#048;&#051;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;">email me</a>.</p>
</div>