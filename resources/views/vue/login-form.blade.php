<div class="row">
    <div class="col-md-6 col-md-offset-4 col-xs-12">
        <div @click='toggleSocialLogin' class='alert alert-info text-center'>
            <span v-show="useSocialLogins">Back</span>
            <span v-else>Log In Using Social Media...</span>
        </div>
    </div>
</div>


<div class="social-logins" v-if="useSocialLogins">
    <div class="row">
        <div class="col-md-6 col-md-offset-4 col-xs-12">
            {{--<a href="{{ route("socialite.login", "facebook") }}" class="btn btn-primary btn-block">
                <i class="fa fa-facebook"></i> Login with Facebook</a>--}}

            <a href="{{ route("socialite.login", "twitter") }}" class="btn btn-primary btn-block">
                <i class="fa fa-twitter"></i> Login with Twitter</a>

           {{-- <a href="{{ route("socialite.login", "google") }}" class="btn btn-primary btn-block">
                <i class="fa fa-google"></i> Login with Google</a>--}}
        </div>
    </div>
</div>

<form v-else class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
    {!! csrf_field() !!}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">E-Mail Address</label>

        <div class="col-md-6">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}">

            @if ($errors->has('email'))
                <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Password</label>

        <div class="col-md-6">
            <input type="password" class="form-control" name="password">

            @if ($errors->has('password'))
                <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"> Remember Me
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-sign-in"></i> Login
            </button>

            <a class="btn btn-link" v-link="{ path: '/reset' }">Forgot Your Password?</a>
        </div>
    </div>
</form>