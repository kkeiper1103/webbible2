<div class="notes" v-show="user.name != null && notes.length">
    <h3 class="title"><i class="fa fa-sticky-note"></i> Notes</h3>

    <ul class="list-unstyled">
        <li v-for="n in notes">
            @{{ n | json }}
        </li>
    </ul>
</div>
