<form class="form" action="{{ url('search') }}" v-on:submit="submit" method="get">
    {!! csrf_field() !!}

    <div class="field-group">
        <div class="input-group">
            <input type="search" name="terms" id="terms" class="form-control" value="@{{ terms }}" placeholder="Search Terms">

            <div class="input-group-btn">
                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>
</form>

