<div class="row legend">
    <div class="col-md-8">
        <h4>Old Testament</h4>

        <div class="row">
            <div class="col-md-3">
                <strong>Pentateuch</strong>

                <ul class="list-unstyled">
                    @foreach( $books->slice(0, 5) as $book )
                        <li><a v-link="{ path: '/books/{{ $book->id }}', activeClass: 'active' }">{{ $book->name }}</a></li>
                    @endforeach
                </ul>

            </div>
            <div class="col-md-3">
                <strong>Historical</strong>

                <ul class="list-unstyled">
                    @foreach( $books->slice(5, 12) as $book )
                        <li><a v-link="{ path: '/books/{{ $book->id }}', activeClass: 'active' }">{{ $book->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <strong>Poetical</strong>

                <ul class="list-unstyled">
                    @foreach( $books->slice(17, 5) as $book )
                        <li><a v-link="{ path: '/books/{{ $book->id }}', activeClass: 'active' }">{{ $book->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <strong>Prophetical</strong>

                <ul class="list-unstyled">
                    @foreach( $books->slice(22, 17) as $book )
                        <li><a v-link="{ path: '/books/{{ $book->id }}', activeClass: 'active' }">{{ $book->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <h4>New Testament</h4>

        <div class="row">
            <div class="col-md-6">
                <strong>Gospels</strong>

                <ul class="list-unstyled">
                    @foreach( $books->slice(39, 4) as $book )
                        <li><a v-link="{ path: '/books/{{ $book->id }}', activeClass: 'active' }">{{ $book->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
                <strong>Epistles</strong>

                <ul class="list-unstyled">
                    @foreach( $books->slice(43) as $book )
                        <li><a v-link="{ path: '/books/{{ $book->id }}', activeClass: 'active' }">{{ $book->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>


