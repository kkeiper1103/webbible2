<nav class="navbar navbar-default yamm">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation"
                    aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">Web Bible</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-navigation">
            <ul class="nav navbar-nav">
                <li class="dropdown yamm-fw" v-link-active>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Books <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                @include('partials._nav-mega')
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown" v-link-active>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Random <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a v-link="{ path: '/random/book' }">Book</a></li>
                        <li><a v-link="{ path: '/random/chapter' }">Chapter</a></li>
                        <li><a v-link="{ path: '/random/verse' }">Verse</a></li>
                        <li class="divider"></li>
                        <li><a v-link="{ path: '/random/old-testament' }">Old Testament</a></li>
                        <li><a v-link="{ path: '/random/new-testament' }">New Testament</a></li>
                        <li><a v-link="{ path: '/random/gospels' }">Gospels</a></li>
                        <li><a v-link="{ path: '/random/epistles' }">Epistles</a></li>
                    </ul>
                </li>
                <li v-link-active><a v-link="{ path: '/search', activeClass: 'active' }">Search</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <template v-if="logged_in">
                    <li v-link-active><a v-link="{ path: '/account', activeClass: 'active' }">Account</a></li>
                    <li><a href="{{ url('logout') }}">Log Out</a></li>
                </template>

                <template v-else>
                    <li v-link-active><a v-link="{ path: '/register', activeClass: 'active' }">Register</a></li>
                    <li v-link-active><a v-link="{ path: '/login', activeClass: 'active' }">Log In</a></li>
                </template>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>