<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title v-text="title">Web Bible - All Books of the Bible</title>

    <link rel="icon" type="image/png" href="{{ url('favicon.png') }}">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>


    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">


    @if( Auth::check() && Auth::user()->preferences )
        <link rel="stylesheet" href="{{ Auth::user()->preferences->stylesheet }}">
    @endif

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">


    @if( env('APP_ENV', 'production') !== 'production' )
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/1.0.20/vue.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue-router/0.7.10/vue-router.js"></script>
        <script>Vue.config.debug = true;</script>
    @else
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/1.0.20/vue.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue-router/0.7.10/vue-router.min.js"></script>
    @endif


    @if( env('APP_ENV', 'production') == 'production' )
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-52345785-1', 'auto');
            ga('send', 'pageview');
        </script>
    @endif

    <script>
        const AuthUser = {!! Auth::check() ? Auth::user() : '{ "name": null, "email": null }' !!};

        const LoggedIn = {!! json_encode(Auth::check()) !!};

        const csrfToken = "{!! csrf_token() !!}";

        const AppEnv = '{{ env('APP_ENV', 'production') }}';

        @if( Auth::check() && Auth::user()->preferences )
        const AppTimezone = '{{ Auth::user()->preferences->timezone }}';
        @endif


        Vue.config.debug = false;
    </script>

    <style>
        .bounce-transition {
            transition: all .5s ease;
            opacity: 100;
        }

        .bounce-enter, .bounce-leave {
            opacity: 0;
        }
    </style>

    @if( (Auth::check() && Auth::user()->preferences) && preg_match('/(darkly|slate|cyborg|superhero)/i', Auth::user()->preferences->stylesheet) === 1 )
        <style>
            .footer-container {
                background-color: #333;
                color: white;
            }

            .copyright-container {
                background-color: #131313;
                color: white;
            }
        </style>
    @endif

</head>
<body id="app-layout">


<div class="nav-container">
    @include('partials._nav')
</div>

@inject('request', 'Illuminate\Http\Request')
@if( $request->session()->has('status') )
<div class="flash-container">
    <div class="container">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ $request->session()->get('status') }}
        </div>
    </div>
</div>
@endif

@if( $request->session()->has('danger') )
    <div class="container">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ $request->session()->get('danger') }}
        </div>
    </div>
@endif


<div class="content-container" id="content-container">
    <main class="container" id="content">
        @yield('content')
    </main>
</div>



<div class="footer">
    <div class="toggle-container bg-primary">
        <div class="container text-center">
            <div class="toggle">
                <i class="fa fa-chevron-up"></i>
            </div>
        </div>
    </div>

    <div class="footer-container">
        <div class="navigation container">
            @include('partials._nav-mega')
        </div>
    </div>

    <div class="copyright-container">
        <div class="container">
            <footer>
                <p class="text-center">Application © {{ date('Y') }} · Kyle Keiper · All Rights Reserved · Provided Free of Charge AND Free of
                    Ads</p>
            </footer>
        </div>
    </div>
</div>

@foreach( $vue_templates as $v )
<script id="templates-{{ str_replace('.', '-', $v) }}" type="text/html">@include( "vue.{$v}" )</script>
@endforeach

    <!-- JavaScripts -->
    <script src="//code.jquery.com/jquery-2.2.2.min.js"
        integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI="
        crossorigin="anonymous"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.3/moment-timezone-with-data.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.7/hammer.min.js"></script>
    <script src="{{ url('store.min.js') }}"></script>

    <script src="{{ elixir('js/all.js') }}"></script>

</body>
</html>
