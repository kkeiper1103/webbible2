@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-9">
            <router-view></router-view>
        </div>
        <div class="col-md-3">
            <bookmarks></bookmarks>
            <recent-chapters></recent-chapters>
        </div>
    </div>
@stop