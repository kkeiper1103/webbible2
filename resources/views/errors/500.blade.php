@extends("layouts.app")

@section('content')
    <h1>Uh Oh. Something Went Wrong.</h1>
    <h2>An Error Occurred While Processing This Request. Please Try Again.</h2>
@stop