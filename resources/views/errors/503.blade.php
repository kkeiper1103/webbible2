<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>Web Bible - Down for Maintenance</title>

    <link rel="icon" type="image/png" href="{{ url('favicon.png') }}">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>


    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    @if( Auth::check() )
        <link rel="stylesheet" href="{{ Auth::user()->preferences->stylesheet }}">
    @endif

    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <style>
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            /* Margin bottom by footer height */
            margin-bottom: 50px;
        }

        .copyright-container {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 50px;
            background-color: #f5f5f5;
        }
    </style>

    @if( env('APP_ENV', 'production') == 'production' )
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-52345785-1', 'auto');
            ga('send', 'pageview');
        </script>
    @endif

</head>
<body id="app-layout">


<div class="content-container" id="content-container">
    <main class="container" id="content">
        <h1 class="title">The Web Bible is Currently Down for Maintenance.</h1>
        <h2>Check Back Soon!</h2>
    </main>
</div>



<div class="copyright-container">
    <div class="container">
        <footer>
            <p class="text-center">Application © {{ date('Y') }} · Kyle Keiper · All Rights Reserved · Provided Free of Charge AND Free of
                Ads</p>
        </footer>
    </div>
</div>

            <!-- JavaScripts -->
    <script src="//code.jquery.com/jquery-2.2.2.min.js"
            integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI="
            crossorigin="anonymous"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>
