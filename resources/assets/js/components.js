/**
 * Created by kkeiper1103 on 4/1/16.
 */

/**
 *
 * @type {Function|void}
 */
var IndexComponent = Vue.extend({
    template: '#templates-root',

    data: function() {
        return {

        };
    },

    attached: function() {
        this.$dispatch("set-title", "Web Bible - All Books of the Bible");
    }
});

/**
 *
 * @type {Function|void}
 */
var BooksComponent = Vue.extend({
    template: '#templates-books',

    data: function() {
        return {
            books: []
        };
    },

    route: {
        waitForData: true,

        data: function(transition) {

            var query = {
                'version': this.$route.query.version
            };


            var key = '/json/books::' + JSON.stringify(query),
                books;


            if( typeof (books = store.get(key)) === 'undefined' ) {
                jQuery.getJSON('/json/books', query, function(data) {
                    this.books = data;

                    store.set(key, this.books);
                }.bind(this));
            }
            else {
                this.books = books;
            }


            this.$dispatch('set-title', "Web Bible - All Books of the Bible");

            transition.next();
        }
    }
});


/**
 *
 * @type {Function|void}
 */
var BookComponent = Vue.extend({
    template: '#templates-books-book',

    data: function() {
        return {
            book: [],
            shown: true
        };
    },

    route: {
        waitForData: true,
        data: function(transition) {
            var query = {
                'version': this.$route.query.version
            };


            var url = '/json/books/' + this.$route.params.book_id,
                key = url + '::' + JSON.stringify(query),
                book;


            if( typeof (book = store.get(key)) === 'undefined' ) {
                jQuery.getJSON(url, query, function(data) {
                    processBookData.apply(this, [data]);

                    store.set(key, data);
                }.bind(this));
            }
            else {
                processBookData.apply(this, [book]);
            }

            //
            function processBookData(book) {
                this.book = book;

                this.$root.$set('max_chapters', book.chapters.length);
                this.$root.$broadcast('toggle:chapterlist', false);
            }

            transition.next();
        }
    },

    events: {
        'toggle:chapterlist': function(state) {
            this.shown = state;
        }
    },

    methods: {
        toggle: function() {
            this.$emit('toggle:chapterlist', !this.shown);
        }
    }
});

/**
 *
 * @type {Function|void}
 */
var ChapterComponent = Vue.extend({
    template: '#templates-books-chapter',

    data: function() {
        return {
            chapter: {
                book: {}
            },
            current_verse: this.$route.params.verse_id || null,
            logged_in: LoggedIn
        };
    },

    route: {
        waitForData: true,
        data: function(transition) {

            // set current verse up here because it relies on the route, not the json
            this.current_verse = this.$route.params.verse_id || null;


            var query = {
                'version': this.$route.query.version
            };


            var url = '/json/books/' + this.$route.params.book_id + '/chapters/' + this.$route.params.chapter_id,
                key = url + '::' + JSON.stringify(query),
                chapter;


            /**
             *
             */
            if( typeof (chapter = store.get(key)) === 'undefined' ) {
                jQuery.getJSON(url, query, function(chapter) {
                    store.set(key, chapter);

                    processChapterData.apply(this, [chapter]);
                }.bind(this));
            }
            else {
                processChapterData.apply(this, [chapter]);
            }

            /**
             *
             * @param chapter
             */
            function processChapterData(chapter) {
                this.chapter = chapter;
                this.$dispatch('set-title', "Web Bible - " + chapter.book.name + " " + chapter.number);

                this.$nextTick(function() {
                    jQuery('[data-toggle="tooltip"]').tooltip({ 'html': true });
                });

                this.$root.$broadcast('toggle:booklist', false);
            }

            transition.next();
        }
    },

    methods: {
        createBookmark: function( verse ) {
            var ajx = jQuery.ajax('/json/bookmarks', {
                data: {
                    verse_id: verse.id,
                    _token: csrfToken
                },
                method: 'post'
            });

            //
            ajx.done(function(d) {
                this.$root.$broadcast('bookmark.create', d);
            }.bind(this));
        },

        createNote: function( verse ) {
            return console.log( verse );

            var ajx = jQuery.ajax('/json/notes', {
                data: {
                    verse_id: verse.id,
                    _token: csrfToken
                },
                method: 'post'
            });

            //
            ajx.done(function(d) {
                this.$broadcast('note.create', d);
            }.bind(this));
        }
    }
});

/**
 *
 * @type {Function|void}
 */
var BooksListComponent = Vue.extend({
    template: '#templates-general-books-list',

    data: function() {
        return {
            books: [],
            shown: true
        };
    },

    created: function() {

        var query = {
            'version': this.$route.query.version
        };

        jQuery.getJSON('/json/books', query, function(books) {
            this.books = books;
        }.bind(this));
    },

    events: {
        'toggle:booklist': function(state) {
            this.shown = state;
        }
    },

    methods: {
        toggle: function() {
            this.$emit('toggle:booklist', !this.shown);
        }
    }
});

Vue.component('books-list', BooksListComponent);

/**
 *
 * @type {Function|void}
 */
var MissingPageComponent = Vue.extend({
    template: '<p>Page Missing!</p>'
});

/**
 *
 */
var SearchFormComponent = Vue.component('search-form', {
    template: '#templates-search-form',

    data: function() {
        return {

        }
    },

    methods: {
        submit: function(evt) {
            evt.preventDefault();

            var $form = jQuery(evt.target),
                data = {};

            /**
             *
             */
            $.each($form.serializeArray(), function() {
                if( data[this.name] !== undefined ) {

                    if(!data[this.name].push) {
                        data[this.name] = [data[this.name]];
                    }

                    data[this.name].push(this.value || '');
                }

                else {
                    data[this.name] = this.value || '';
                }
            });


            var settings = {
                method: $form.attr('method') || 'post',
                data: data
            };


            var jq = $.ajax($form.attr('action'), settings);


            jq.done(function(data){
                this.$parent.$set('results', JSON.parse(data));
            }.bind(this));

            return false;
        }
    }
});

/**
 *
 * @type {Function|void}
 */
var SearchComponent = Vue.extend({
    template: '#templates-search',

    data: function() {
        return {
            results: {}
        };
    },

    attached: function() {
        this.$dispatch("set-title", "Web Bible - Search the Bible");
    }
});

/**
 *
 */
var AccountComponent = Vue.component('account', {
    template: '#templates-account',

    data: function() {
        return {
            user: AuthUser,
            gestures: false
        };
    },

    attached: function() {
        jQuery.get('/json/user', function(user) {
            this.$set('user', user);
            this.$dispatch('set-title', "Web Bible - Account Settings")
        }.bind(this));
    }
});

/**
 *
 */
var RecentChaptersComponent = Vue.component('recent-chapters', {
    template: '#templates-recent-chapters',

    data: function() {
        return {
            chapters: []
        };
    },

    attached: function() {
        jQuery.get('/json/user', function(user) {
            this.$set('chapters', user.recent_chapters);
        }.bind(this));
    }
});


/**
 *
 */
var BookmarksComponent = Vue.component('bookmarks', {
    template: '#templates-bookmarks',

    data: function() {
        return {
            user: AuthUser,

            bookmarks: []
        };
    },

    attached: function() {
        this.refreshBookmarks();
    },

    methods: {
        deleteBookmark: function( bookmark ) {
            jQuery.ajax('/json/bookmarks/' + bookmark.id, {
                method: 'delete',
                data: {
                    _token: csrfToken,
                    id: bookmark.id
                }

            }).done(function(){
                this.refreshBookmarks();
            }.bind(this));
        },

        refreshBookmarks: function() {
            if(LoggedIn) {
                jQuery.getJSON('/json/bookmarks', function(data) {

                    this.bookmarks = data.map(function(bm) {
                        bm.toString = function() {
                            return bm.verse.chapter.book.name + " " + bm.verse.chapter.number + ":" + bm.verse.number;
                        };

                        return bm;
                    });

                }.bind(this));
            }

        }
    },

    events: {
        'bookmark.create': function( bookmark ) {
            this.refreshBookmarks();
        }
    }
});

/**
 *
 */
var NotesComponent = Vue.component('notes', {
    template: '#templates-notes',

    data: function() {
        return {
            user: AuthUser,

            notes: []
        };
    },

    attached: function() {
        jQuery.getJSON('/json/notes', function(data) {
            this.notes = data;
        }.bind(this));
    },

    methods: {
        updateNote: function() {

        },
        deleteNote: function() {

        }
    }
});

/**
 *
 */
var RegisterForm = Vue.component('register-form', {
    template: '#templates-register-form',

    data: function() {
        return {}
    },

    attached: function() {
        this.$dispatch("set-title", "Web Bible - Register for Account");
    }
});

/**
 *
 */
var LoginForm = Vue.component('login-form', {
    template: '#templates-login-form',

    data: function() {
        return {
            useSocialLogins: false
        }
    },

    attached: function() {
        this.$dispatch("set-title", "Web Bible - Log Into Account");
    },

    methods: {
        toggleSocialLogin: function() {
            this.useSocialLogins = !this.useSocialLogins;
        }
    }
});

/**
 *
 */
var RandomComponent = Vue.extend({
    template: '<i class="fa fa-circle-o-notch fa-spin fa-5x"></i>',
    route: {
        waitForData: true,
        data: function(transition) {

            jQuery.get('/json/random', {
                type: this.$route.params.type

            }).done(function(data) {

                var url = ['/books', data.book.id, 'chapters', data.chapter.number].join('/');

                if( data.verse ) {
                    url += '/verses/' + data.verse.number;
                }

                this.$route.router.go( url );

            }.bind(this));

            return transition.next();
        }
    }
});

/**
 *
 * @type {Function|void}
 */
var EmailResetForm = Vue.extend({
    template: '#templates-forms-send-reset-email',

    data: function() {
        return {};
    }
});

/**
 *
 * @type {Function|void}
 */
var ResetPasswordForm = Vue.extend({
    template: '#templates-forms-reset',

    data: function() {
        return {
            token: this.$route.params.token,
            email: this.$route.query.email
        };
    }
});