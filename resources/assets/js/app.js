/**
 * Created by kkeiper1103 on 4/1/16.
 */

jQuery(function($){
    $(document).on('submit', '[data-ajax-form]', function(e){
        e.preventDefault();

        var $form = $(this),
            data = {};

        /**
         *
         */
        $.each($form.serializeArray(), function() {
            if( data[this.name] !== undefined ) {

                if(!data[this.name].push) {
                    data[this.name] = [data[this.name]];
                }

                data[this.name].push(this.value || '');
            }

            else {
                data[this.name] = this.value || '';
            }
        });


        var settings = {
            method: $form.attr('method') || 'post',
            data: data
        };


        var jq = $.ajax($form.attr('action'), settings);


        jq.done(function(data){
            console.log(data);
        });


        return false;
    });


    //
    $(document).on('click focus', '.toggle-container .toggle', function(e){
        var $this = $(this);

        //
        $('.navigation.container', '.footer-container').slideToggle(400, function() {
            $('html, body').animate({
                "scrollTop": $(document).height()
            });


            $this.find('i').toggleClass('fa-chevron-up fa-chevron-down');

            // calculate footer height
            calculateFooterHeight.apply( $this );
        });
    });


    //
    $(document).on('click focus', '.back-to-top', function(e) {
        var $sel = $('html, body');

        // if the active-chapter is maximized, we want to select .wrapper, not html and body
        if( $("#active-chapter").hasClass('maximized')) {
            $sel = $('.wrapper', "#active-chapter");
        }

        return $sel.animate({
            "scrollTop": 0
        });
    });


    //
    $('[data-toggle="tooltip"]').tooltip();

    // initial calculation of footer height
    calculateFooterHeight();
});

/**
 *
 */
function calculateFooterHeight() {
    var $ = jQuery,
        toggleHeight = $('.toggle-container').height(),
        navHeight = $('.footer-container').height(),
        copyrightHeight = $('.copyright-container').height(),
        totalHeight = toggleHeight + navHeight + copyrightHeight;


    $('body').css({
        'margin-bottom': totalHeight + "px"
    });

    $('.footer').css({
        'position': 'absolute',
        'bottom': 0,
        'width': '100%',

        'height': totalHeight + "px"
    });
}