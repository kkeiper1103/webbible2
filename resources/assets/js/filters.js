/**
 * Created by kkeiper1103 on 4/5/16.
 */

Vue.filter('embolden', function(value, terms) {
    terms = terms.split(' ');

    return value.replace( new RegExp('(\\b' + terms.join('|') + '\\b)', 'mgi'), '<strong>$1</strong>');
});


Vue.filter('date', function(value, date) {
    var timezone = typeof AppTimezone !== 'undefined' ? AppTimezone : 'UTC';

    return moment(value).tz(timezone).format(date);
});