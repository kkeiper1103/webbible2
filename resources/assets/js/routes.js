/**
 * Created by kkeiper1103 on 4/1/16.
 */

delete Hammer.defaults.cssProps.userSelect;

var App = Vue.extend({
        data: function() {
            return {
                logged_in: LoggedIn,
                title: "Web Bible - Home",
                max_chapters: 1
            }
        },

        events: {
            'set-title': function(title) {
                this.title = title;
            }
        }
    }),

    router = new VueRouter({
        transitionOnLoad: true
    }),


    hammertime = new Hammer.Manager(document.getElementById('content-container'), {});



hammertime.add( new Hammer.Swipe({ direction: Hammer.DIRECTION_HORIZONTAL }) );


hammertime.on('swipe', function(e) {
    var $route = router.app.$route,
        params = {};

    // don't handle if we're not on the verses page.
    if($route.name !== "verses")
        return;




    if(e.direction == Hammer.DIRECTION_LEFT) {
        params = {
            book_id: parseInt($route.params.book_id),
            chapter_id: Math.min(parseInt($route.params.chapter_id) + 1, router.app.max_chapters)
        };
    }
    else if(e.direction == Hammer.DIRECTION_RIGHT) {
        params = {
            book_id: parseInt($route.params.book_id),
            chapter_id: Math.max(parseInt($route.params.chapter_id) - 1, 1)
        };
    }


    router.go({
        name: 'verses',
        params: params
    });
});


if( LoggedIn && AuthUser.preferences && AuthUser.preferences.allow_gestures == 1 ) {
    gest.start();


    gest.options.subscribeWithCallback(function(gesture) {

        var map = {
            'Left': Hammer.DIRECTION_LEFT,
            'Right': Hammer.DIRECTION_RIGHT,
            /*'Up': Hammer.DIRECTION_UP,
            'Long up': Hammer.DIRECTION_UP,
            'Down': Hammer.DIRECTION_DOWN,
            'Long down': Hammer.DIRECTION_DOWN*/
        };

        // if a gesture matches the map
        if( map.hasOwnProperty(gesture.direction) ) {
            hammertime.emit('swipe', {
                direction: map[gesture.direction]
            });
        }
    });
}






/**
 *
 */
router.map({
    '/': {
        component: IndexComponent,
        title: 'Web Bible - Home',
        name: 'index'
    },
    '/books': {
        component: BooksComponent,
        title: 'Web Bible - Books',
        name: 'books',

        subRoutes: {
            '/:book_id/chapters': {
                component: BookComponent,
                title: 'Web Bible - Chapters',
                name: 'chapters',

                subRoutes: {
                    '/:chapter_id/verses': {
                        component: ChapterComponent,
                        title: 'Web Bible - Verses',
                        name: 'verses',

                        subRoutes: {
                            '/:verse_id': {
                                component: ChapterComponent,
                                title: 'Web Bible - Verses',
                                name: 'verse'

                            }
                        }
                    }
                }
            }
        }
    },

    '/search': {
        component: SearchComponent,
        title: "Search"
    },

    '/account': {
        component: AccountComponent,
        title: "Account"
    },

    '/register': {
        component: RegisterForm,
        title: "Register Account"
    },

    '/login': {
        component: LoginForm,
        title: "Log In"
    },

    '/reset': {
        component: EmailResetForm,
        title: "Reset Password"
    },

    '/reset/password/:token': {
        component: ResetPasswordForm,
        title: "Reset Password"
    },

    '/random/:type': {
        component: RandomComponent,
        title: "Random Passage"
    }
});


/**
 *
 */
router.alias({
    '/books/:book_id': '/books/:book_id/chapters',
    '/books/:book_id/chapters': '/books/:book_id/chapters/1',
    '/books/:book_id/chapters/:chapter_id': '/books/:book_id/chapters/:chapter_id/verses',
    '*': '/books'
});


/**
 * set up tracking
 */
if( AppEnv === "production" ) {
    router.afterEach(function(transition) {
        ga('set', 'page', transition.to.path);
        ga('send', 'pageview');
    });
}


/**
 *
 */
if( location.pathname == '/' )
    router.start(App, 'html');