/**
 * Created by kkeiper1103 on 4/1/16.
 */

jQuery(function($) {

    $(document).on('click touch tap', '[data-toggle="fullscreen"]', function(e) {

        var $chapter = $('#active-chapter');

        //
        $chapter.toggleClass('maximized');


        $chapter.css({
            backgroundColor: $chapter.hasClass('maximized') ?
                $('body').css('background-color') :
                'inherit'
        });


        $(this).html( !($chapter.hasClass('maximized')) ?
            '<i class="fa fa-expand"></i> Expand' :
            '<i class="fa fa-compress"></i> Return'
        );

    });

});