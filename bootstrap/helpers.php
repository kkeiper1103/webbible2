<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/1/16
 * Time: 1:19 PM
 */

/**
 * @param $start
 * @return array
 */
function get_views( $start ) {
    $files = array_diff(scandir( $start ), ['.', '..']);


    $views = array_flatten(
        array_values(
            array_map(function($f) use($start){
                if( is_dir($start . DIRECTORY_SEPARATOR . $f) ) {
                    $views = get_views($start . DIRECTORY_SEPARATOR . $f);

                    return array_map(function($ff) use($f) {
                        return "{$f}.{$ff}";
                    }, $views);
                }

                return $f;
            }, $files)
        )
    );

    return array_map(function($v){
        return str_replace('.blade.php', '', $v);
    }, $views);
}