<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariableDatabaseModel extends Model
{
    /**
     * Book constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {

        // if logged in, go ahead and set the connection
        $this->connection = app('bible.version');

        return parent::__construct($attributes);
    }
}
