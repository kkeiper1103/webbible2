<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class CreateSchemaForNewBibleVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bible:schema {version}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the Default Schema for a New Bible Version.';


    protected $sql_template = <<<EOS
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE DATABASE IF NOT EXISTS `{{database}}`;
USE `{{database}}`;


CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `chapters` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `verses` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` int(11) NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `books_name_unique` (`name`);


ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `verses`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

ALTER TABLE `chapters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

ALTER TABLE `verses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;



EOS;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $version = $this->argument('version');

        if( empty($version) ) { return; }

        $replacements = [
            'database' => env('DB_DATABASE') . "_{$version}"
        ];

        //
        foreach( $replacements as $var => $value ) {
            $this->sql_template = str_replace('{{' . $var . '}}', $value, $this->sql_template);
        }

        /**
         *
         */
        DB::transaction(function() use($replacements, $version) {

            // create database
            $result = DB::connection()->getPdo()->exec( $this->sql_template );

            if( $result === false) {
                throw new \Exception("Could Not Create Database For Some Reason.");
            }

            $this->info("Please Make Sure You Configure the New Database in 'config/database.php'.");
        });

    }
}
