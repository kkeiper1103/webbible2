<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class ImportZefaniaFormatBible extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:zefania {path} {connection}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import a USFX Format Bible (XML Based Bible)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');
        $conn = $this->argument('connection');

        $xml = simplexml_load_file($path);

        // set database connection
        app()->singleton('bible.version', function() use($conn){
            return $conn;
        });


        DB::connection($conn)
            ->getPdo()
            ->exec("ALTER TABLE `books` AUTO_INCREMENT=0; ALTER TABLE `chapters` AUTO_INCREMENT=0; ALTER TABLE `verses` AUTO_INCREMENT=0;");


        $bar = $this->output->createProgressBar( count($xml->xpath('/XMLBIBLE/BIBLEBOOK/CHAPTER')) );

        // books
        foreach( $xml->xpath("/XMLBIBLE/BIBLEBOOK") as $book ) {
            $book_title = (string) $book['bname'];

            //
            $b = \App\Book::create([
                'name' => $book_title
            ]);

            // book.chapters
            foreach($book->xpath('CHAPTER') as $chapter) {
                $c_num = (string) $chapter['cnumber'];

                /**
                 * @var \App\Chapter $c
                 */
                $c = $b->chapters()->create([
                    'number' => $c_num
                ]);

                // book.chapter.verses
                foreach( $chapter->xpath('VERS') as $verse ) {

                    $c->verses()->create([
                        'number' => $verse['vnumber'],
                        'value' => (string) $verse
                    ]);
                }

                $bar->advance();
            }
        }

        $bar->finish();

        $this->info("\nFinished Importing New Bible Version.");
    }
}
