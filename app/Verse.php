<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verse extends VariableDatabaseModel
{
    protected $fillable = [
        'number', 'value'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chapter() {
        return $this->belongsTo(Chapter::class);
    }
}
