<?php

namespace App\Providers;

use App\User;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Guard $auth)
    {
        require_once base_path('bootstrap/helpers.php');

        view()->composer('partials._nav-mega', function($view) {
            return $view->withBooks( \App\Book::all() );
        });

        view()->composer('layouts.app', function($view) {
            return $view->withVueTemplates(get_views( resource_path('views/vue') ));
        });

        //
        Validator::extend('authentic', function($attribute, $value, $parameters, $validator) {
            return \Hash::check($value, Auth::user()->getAuthPassword());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if( env("APP_ENV") !== "production" ) {
            $this->app->register( \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class );
            // $this->app->register( \Barryvdh\Debugbar\ServiceProvider::class );
            $this->app->register( \Laracasts\Generators\GeneratorsServiceProvider::class );
        }
    }
}
