<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class SetBibleVersion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $connection = 'kjv';

        if( ($c = $request->input('version', false)) !== false ) {
            $connection = $c;
        }
        else if( Auth::check() && Auth::user()->preferences ) {
            $connection = Auth::user()->preferences->connection;
        }

        app()->singleton('bible.version', function($app) use($connection){
            return $connection;
        });

        return $next($request);
    }
}
