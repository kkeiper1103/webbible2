<?php

namespace App\Http\Middleware;

use App\Book;
use Auth;
use Cache;
use Carbon\Carbon;
use Closure;
use DB;

class LogRecentChapter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if( Auth::check() ) {
            list($book, $chapter) = array_values( \Route::current()->parameters() );

            $books = Cache::rememberForever('books', function() {
                return Book::all();
            });

            // grab ref name since we use it muiltiple times
            $ref = sprintf("%s %d", $books->find($book)->name, $chapter);

            // only insert if last reading isn't the same as this reading (prevent double logging)
            // threshold for recent is two weeks
            $last_reading = DB::table('recent_chapters')
                ->select('*')
                ->where('user_id', '=', Auth::id())
                ->where('reference_name', 'LIKE', $ref)
                ->where('created_at', '>', Carbon::now()->subWeek(2))
                ->orderBy('created_at', 'desc')
                ->get();

            if( count($last_reading) < 1 ) {
                DB::table('recent_chapters')->insert([
                    'book_id' => $book,
                    'chapter_number' => $chapter,
                    'user_id' => Auth::id(),

                    'reference_name' => $ref,

                    'created_at' => Carbon::now( Auth::user()->preferences->timezone ),
                    'updated_at' => Carbon::now( Auth::user()->preferences->timezone )
                ]);
            }
        }

        return $response;
    }
}
