<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Exception;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * @param $provider
     * @return mixed
     */
    public function redirectToProvider( $provider ) {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback( $provider ) {
        try {
            $user = Socialite::driver($provider)->user();
        }
        catch(Exception $e) {
            return redirect()->to("auth/social/{$provider}");
        }

        $user = $this->findOrCreateUser($provider, $user);
        Auth::login($user, true);

        return redirect()->to($this->redirectTo);
    }


    /**
     * @param $provider
     * @param $user
     * @return User
     */
    protected function findOrCreateUser($provider, $user)
    {
        if($authUser = User::where("{$provider}_id", $user->getId())->first()) {
            return $authUser;
        }

        return User::create([
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            "{$provider}_id" => $user->getId()
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
