<?php

namespace App\Http\Controllers\Json;

use App\Book;
use App\Chapter;
use App\Verse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VersesController extends Controller
{
    /**
     * ChaptersController constructor.
     */
    public function __construct() {
        $this->middleware('chapter.log', [
            'only' => 'index'
        ]);
    }

    /**
     * @param Book $book
     * @param Chapter $chapter
     * @return mixed
     */
    public function index($book, $chapter) {
        return Chapter::with('book', 'verses')
            ->where('book_id', $book)
            ->where('number', $chapter)
            ->first();
    }

    /**
     * @param Book $book
     * @param Chapter $chapter
     * @param Verse $verse
     * @return Chapter
     */
    public function show($book, $chapter, $verse) {
        $chapter = Chapter::where('book_id', $book)->where('number', $chapter)->first();

        return $chapter->verses()->where('number', $verse)->first();
    }
}
