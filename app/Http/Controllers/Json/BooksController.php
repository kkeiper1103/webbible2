<?php

namespace App\Http\Controllers\Json;

use App\Book;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BooksController extends Controller
{

    /**
     *
     */
    public function index() {
        return Book::all();
    }

    /**
     * @param $id
     * @return Book
     */
    public function show($id) {
        return redirect()->route('json.books.chapters.index', $id, 301);
    }
}
