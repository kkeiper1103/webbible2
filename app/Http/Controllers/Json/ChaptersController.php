<?php

namespace App\Http\Controllers\Json;

use App\Book;
use App\Chapter;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ChaptersController extends Controller
{

    /**
     * @param Book $book
     * @return mixed
     */
    public function index($book) {
        return Book::with('chapters')->find($book);
    }

    /**
     * @param Book $book
     * @param Chapter $chapter
     * @return Chapter
     */
    public function show($book, $chapter) {
        return redirect()->route('json.books.chapters.verses.index', [$book, $chapter], 301);
    }
}
