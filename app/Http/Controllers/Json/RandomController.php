<?php

namespace App\Http\Controllers\Json;

use App\Book;
use App\Chapter;
use App\Verse;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Cache;

class RandomController extends Controller
{
    public function getIndex(Request $request) {

        //
        switch( $request->input('type') ) {
            case "book":
                $data = $this->book();
                break;
            case 'chapter':
                $data = $this->chapter();
                break;
            case 'verse':
                $data = $this->verse();
                break;
            case 'old-testament':
                $data = $this->ot();
                break;
            case 'new-testament':
                $data = $this->nt();
                break;
            case 'gospels':
                $data = $this->gospels();
                break;
            case 'epistles':
                $data = $this->epistles();
                break;
            default:
                $data = [];
                break;
        }

        return response()->json($data);
    }

    /**
     *
     */
    public function book() {
        $books = \Cache::rememberForever('books', function() {
            return Book::all();
        });

        $book = $this->random($books);

        return [
            'book' => $book,
            'chapter' => $this->random( $book->chapters ),
            'verse' => null
        ];
    }

    /**
     *
     */
    public function chapter() {
        $chapters = \Cache::rememberForever('chapters', function() {
            return Chapter::all();
        });

        $chapter = $this->random( $chapters );

        return [
            'book' => $chapter->book,
            'chapter' => $chapter,
            'verse' => null
        ];
    }

    /**
     *
     */
    public function verse() {
        $verses = \Cache::rememberForever('verses', function() {
            return Verse::all();
        });

        $verse = $this->random( $verses );

        return [
            'book' => $verse->chapter->book,
            'chapter' => $verse->chapter,
            'verse' => $verse
        ];
    }

    /**
     *
     */
    public function ot() {
        $book = $this->random( Book::where('id', '<', 40)->get() );

        return [
            'book' => $book,
            'chapter' => $this->random($book->chapters),
            'verse' => null
        ];
    }

    /**
     *
     */
    public function nt() {
        $book = $this->random( Book::where('id', '>', 39)->get() );

        return [
            'book' => $book,
            'chapter' => $this->random($book->chapters),
            'verse' => null
        ];
    }

    /**
     *
     */
    public function gospels() {
        $book = $this->random( Book::whereIn('id', [
            40, 41, 42, 43
        ])->get() );

        return [
            'book' => $book,
            'chapter' => $this->random($book->chapters),
            'verse' => null
        ];
    }

    /**
     *
     */
    public function epistles() {
        $book = $this->random( Book::whereIn('id', [
            45, 46, 47, 48, 49, 50,
            51, 52, 53, 54, 55, 56,
            57, 58, 59, 60, 61, 62,
            63, 64, 65
        ])->get() );

        return [
            'book' => $book,
            'chapter' => $this->random($book->chapters),
            'verse' => null
        ];
    }

    /**
     * Returns random item from collection
     *
     * @param Collection $collection
     * @return mixed
     */
    public function random( Collection $collection ) {
        return $collection[ mt_rand(0, $collection->count() - 1) ];
    }
}
