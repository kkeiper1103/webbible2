<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//

Route::get('/', function () {
    return view('index');
});


/**
 *
 */
Route::group(['namespace' => 'Json', 'prefix' => 'json'], function(){

    //
    Route::get('/', function(){
        // return some sort of json here to show the routes available
        // maybe some sort of dashboard?

        return [];
    });

    Route::resource('books', 'BooksController', [
        'only' => ['index', 'show']
    ]);

    Route::resource("books.chapters", "ChaptersController", [
        'only' => ['index', 'show']
    ]);

    Route::resource("books.chapters.verses", "VersesController", [
        'only' => ['index', 'show']
    ]);


    // bookmarks and notes
    Route::resource("bookmarks", "BookmarksController", [
        'except' => ['create', 'edit']
    ]);

    Route::resource("notes", "NotesController", [
        'except' => ['create', 'edit']
    ]);


    // account information
    Route::get('user', function() {
        if( Auth::guest() )
            return new App\User;

        /**
         * @var App\User $user
         */
        $user = Auth::user()->load('preferences');

        if( empty($user->preferences) )
            $user->preferences()->save(new \App\Preferences);

        //
        return $user;
    });

    Route::get('user/bookmarks', function() {
        return Auth::check() ? Auth::user()->bookmarks : collect([]);
    });

    Route::get('user/notes', function() {
        return Auth::check() ? Auth::user()->notes : collect([]);
    });

    // random
    Route::get('random', 'RandomController@getIndex');
});


// auth routes
Route::post('register', function(\Illuminate\Http\Request $request) {
    $validator = Validator::make($request->input(), App\User::$rules);

    //
    if($validator->passes()) {
        $user = new App\User($request->input());
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $user->preferences()->create([]);

        //
        $request->session()->flash('success', "Account Created. Please Log In.");

        return redirect()->to('/#!/login');
    }

    return redirect()->to('/#!/register')
        ->withInput( $request->input() )
        ->withErrors( $validator->errors() );
});

//
Route::post('login', function(\Illuminate\Http\Request $request) {
    if( Auth::attempt($request->only('email', 'password'), $request->input('remember', false)) ) {
        return redirect()->to('/#!/');
    }

    $request->session()->flash('danger', 'Invalid Credentials.');

    return redirect()->to('/#!/login');
});




Route::get("auth/social/{provider}", [
    'as' => 'socialite.login',
    'uses' => 'Auth\\AuthController@redirectToProvider'
]);


Route::get("auth/social/{provider}/callback", [
    'as' => 'socialite.callback',
    'uses' => 'Auth\\AuthController@handleProviderCallback'
]);




//
Route::get('logout', function() {
    Auth::logout();

    return redirect()->to('/#!/');
});

// Password Reset Routes...
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');



// don't do sensitive information (passwords, etc) over ajax
Route::post('account', function(\Illuminate\Http\Request $request) {
    $validator = Validator::make($request->input(), [
        'name' => 'required',
        'email' => 'required|email',

        'current_password' => 'authentic|required_with:password',
        'password' => 'confirmed'
    ]);

    //
    if( $validator->fails() ) {
        return redirect()->to('/#!/account')
            ->withInput($request->input())
            ->withErrors($validator->errors());
    }

    $user = Auth::user();
    $user->fill($request->input());

    // let's do password
    if( !empty( $password = $request->input('password', false) ) ) {
        $user->password = bcrypt($password);
    }

    // setting preferences
    $user->preferences->stylesheet = $request->input('stylesheet');
    $user->preferences->connection = $request->input('connection', 'kjv');
    $user->preferences->timezone = $request->input('timezone', 'UTC');
    $user->preferences->allow_gestures = $request->input('allow_gestures', false);

    if( $user->push() ) {
        $request->session()->flash('status', "Successfully Saved Account Settings.");
    }

    return redirect()->to('/#!/account');
});


// search routes
Route::get("search", function(\Illuminate\Http\Request $request) {

    $results = [
        'query' => [
            'terms' => $request->input('terms'),
            'matches' => 0
        ],

        'exact' => [],
        'partial' => []
    ];


    // weighted exact match
    $search_terms = $request->input('terms');

    $verses = \App\Verse::with('chapter', 'chapter.book')->where('value', 'REGEXP', "[[:<:]]{$search_terms}[[:>:]]")
        ->take(50)->get();

    $results['exact'] = $verses;
    $results['query']['matches'] += $verses->count();


    // lesser weighted all words, but no order match
    $search_terms = explode(' ', $search_terms);
    $query = App\Verse::with('chapter', 'chapter.book');

    foreach ($search_terms as $search_term) {
        $query->where('value', 'REGEXP', "[[:<:]]{$search_term}[[:>:]]");
    };

    $results['partial'] = $query->take(50)->get();
    $results['query']['matches'] += $results['partial']->count();




    // filter out the results
    foreach( $results['partial'] as $i => $result ) {
        if( $results['exact']->contains($result) ) {
            $results['partial'] = $results['partial']->diff([$result]);
        }
    }

    return json_encode($results);
});

// legacy routes for redirecting to new locations
Route::get('{book}/{chapter}/{verse?}', ['as' => 'legacy-reference', 'uses' => function($book, $chapter, $verse = null) {

    //
    if( ($book = \App\Book::where('name', 'LIKE', $book)->first()) == null ) {
        abort(404);
    }

    //
    if( ($chapter = $book->chapters()->where('number', $chapter)->first()) == null ) {
        abort(404);
    }

    $parts = [
        '/#!/books/',
        $book->id,
        '/chapters/',
        $chapter->number
    ];

    //
    if( $verse ) {
        $verse = $chapter->verses()->where('number', $verse)->first();

        $parts[] = '/verses/';
        $parts[] = $verse->number;
    }

    return redirect()->to( implode('', $parts), 301 );
}]);