<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends VariableDatabaseModel
{
    protected $fillable = [
        'number'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book() {
        return $this->belongsTo(Book::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function verses() {
        return $this->hasMany(Verse::class);
    }
}
