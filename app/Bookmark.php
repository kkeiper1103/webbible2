<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'verse_id'
    ];

    protected $with = [
        'verse', 'verse.chapter', 'verse.chapter.book'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo( User::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function verse() {
        return $this->belongsTo(Verse::class);
    }
}
