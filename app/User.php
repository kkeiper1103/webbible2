<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'facebook_id', 'google_id', 'twitter_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|confirmed'
    ];

    // add non column data to model
    public $appends = [
        'recent_chapters'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function preferences() {
        return $this->hasOne(Preferences::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes() {
        return $this->hasMany(Note::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookmarks() {
        return $this->hasMany(Bookmark::class);
    }




    /**
     * @return array|static[]
     */
    public function getRecentChaptersAttribute() {
        return $this->recent_chapters();
    }



    /**
     * @return array|static[]
     */
    public function recent_chapters() {
        return \DB::table('recent_chapters')->where('user_id', '=', $this->id)
            ->where('created_at', '>', Carbon::now()->subWeek(2))
            ->orderBy('created_at', 'desc')
            ->take(20)
            ->get();
    }
}
