<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preferences extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'stylesheet', 'last_chapter', 'connection'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
