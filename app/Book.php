<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends VariableDatabaseModel
{
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chapters() {
        return $this->hasMany(Chapter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function verses() {
        return $this->hasManyThrough(Verse::class, Chapter::class);
    }
}
