<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'body', 'verse_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo( User::class );
    }

    /**
     *
     */
    public function verse() {
        return $this->belongsTo( Verse::class );
    }
}
