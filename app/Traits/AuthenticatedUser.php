<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/21/16
 * Time: 10:21 AM
 */

namespace App\Traits;


trait AuthenticatedUser
{
    /**
     * @return \App\User
     */
    public function getTestingUser() {
        $user = \App\User::firstOrCreate([
            'name' => $this->authorizedName(),
            'email' => $this->authorizedEmail()
        ]);

        if( empty($user->password) ) {
            $user->password = bcrypt( $this->authorizedPassword() );
            $user->save();
        }

        return $user;
    }


    /**
     * @return string
     */
    protected function authorizedPassword()
    {
        return property_exists($this, 'password')
            ? $this->password : 'password';
    }

    /**
     * @return string
     */
    protected function authorizedEmail() {
        return property_exists($this, 'email')
            ? $this->email : 'testing@gnarlyweb.com';
    }

    /**
     * @return string
     */
    protected function authorizedName() {
        return property_exists($this, 'name')
            ? $this->name : 'testing_user';
    }
}