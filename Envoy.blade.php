@servers([ "web" => "kkeiper@e-bible.us" ])


@setup
    $branch = isset($branch) ? $branch : "master";
@endsetup


@macro('deploy', ['on' => 'web'])
    update_code
    migrate
    set_permissions
@endmacro


@task('update_code')
    cd /var/www/vhosts/webbible2
    git pull origin {{ $branch or "master" }}
@endtask



@task('migrate')
    php artisan migrate
@endtask



@task('set_permissions')
    chown www-data:kkeiper . -Rf
    chmod g+rw . -Rf
@endtask